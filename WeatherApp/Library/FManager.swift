//
//  FManager.swift
//  SwiftProject
//
//  Created by Vlad Melnyk on 11.08.18.
//  Copyright © 2018 Vlad Melnyk. All rights reserved.
//

import UIKit

class FManager: ViewController {

    
    enum FileType: Int {
        case FileTypeMutableArray = 0
        case FileTypeMutableDictionary
        case FileTypeString
    }
    
    
    
    static func pathToDocumentsFolder() -> String {
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    }
    
    static func pathToFile(withName name: (String?)) -> NSURL? {
        let documentPath = FManager.pathToDocumentsFolder()
        
        if let fileName = name {
            return NSURL(fileURLWithPath: documentPath).appendingPathComponent("\(fileName).txt") as NSURL?
        }else {
            return nil
        }
        
    }
    
    
    static func createEmptyFile(withFileClasification clasification: (FileType), forPath path: (URL)) {
        switch clasification {
        case .FileTypeMutableDictionary:
            let file : (NSMutableDictionary) = NSMutableDictionary.init()
            file.write(to: path, atomically: true)
            break
        case .FileTypeMutableArray:
            let file : (NSMutableArray) = NSMutableArray.init()
            file.write(to: path, atomically: true)
            break
        case .FileTypeString:
            let file : (String) = String.init()
            do{
                try file.write(to: path, atomically: true, encoding: .unicode)
            }catch{}
            
            break
        }
    }
    
    
    static func getInfoFromFile(withPath path:(URL), andFileType type: (FileType)) -> Any? {
        switch type {
            
        case .FileTypeMutableDictionary:
            
            if let file : (NSMutableDictionary) = NSMutableDictionary.init(contentsOf: path) {
                return file
            }else{
                return nil
            }
            
        case .FileTypeMutableArray:
            if let file : (NSMutableArray) = NSMutableArray.init(contentsOf: path) {
                return file
            }else{
                return nil
            }
        case .FileTypeString:

            do{
                let file : (String) = try String.init(contentsOf: path)
                return file
            }catch{
                return nil
            }
        }
    }
    
    
    static func showPathToFile(withName name: (String?)) {
        let path = FManager.pathToFile(withName: name)
        print("Path to file: \(path)")
    }
    
    
    
//    static func write(image: (UIImage?), atPath path: (URL?)) {
//        if let currentImage = image, let currentpath = path {
//           let imageData :(Data?) = UIImagePNGRepresentation(currentImage)
//            
//            if let propertyData = imageData{
//                do {
//                    try propertyData.write(to: currentpath)
//                }catch {}
//            }
//           
//        }
//    }
    
    
    static func readImage(fromPath path:(URL?)) -> UIImage? {
        if let currentPath = path{
            do{
                let fileData = try Data.init(contentsOf: currentPath)
                
//                if let imageData = fileData {
                    return UIImage.init(data: fileData)
//                }
                
            }catch{ return nil }
        }else {
            return nil
        }
    }
    
    
    
    
    
    

}
