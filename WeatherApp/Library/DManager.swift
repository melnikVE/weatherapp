//
//  DManager.swift
//  SwiftProject
//
//  Created by Vlad Melnyk on 11.08.18.
//  Copyright © 2018 Vlad Melnyk. All rights reserved.
//

import UIKit

class DManager: ViewController {

    static func isEmpty(value: (NSObject?)) -> Bool {
        if let _ = value {
            return false
        }else{
            return true
        }
    }

    
    static func standartAlert (withTitle title: String, message : String) -> UIAlertController{
        let alert : (UIAlertController) = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        return alert
    }
    
//    @available(iOS 10, *)
    static func makeTapticAction() {
        let generator = UISelectionFeedbackGenerator.init()
        generator.prepare()
        generator.selectionChanged()
    }
    
    
    // MARK: - UITableViewCell STACK
    
    static func cellKey(withIndexPath indexPath: (IndexPath)) -> String {
        return "\(indexPath.row)"
    }
    
    static func cell(fromCellStack stack: (NSMutableDictionary), withIndexPath indexPath:(IndexPath)) -> UITableViewCell {
        let cell : (UITableViewCell) = stack.value(forKey: DManager.cellKey(withIndexPath: indexPath)) as! (UITableViewCell)
        return cell
    }
    
    static func save(cell: (UITableViewCell), toCellStack stack: (NSMutableDictionary), forIndexPath indexPath: (IndexPath)){
        stack.setValue(cell, forKey: DManager.cellKey(withIndexPath: indexPath))
    }
    
    
    
    // MARK: - DATE
    
    static func transformToLocal(date:(String), withFromat format: (String)) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let localDate = dateFormatter.date(from: date)
        
        return localDate
    }
    
    
    static func transform(date:(Date), toStringWithFormat format:(String)) -> String{
        let formatter = DateFormatter.init()
        formatter.dateFormat = format
        formatter.timeZone   = NSTimeZone.init(name: "UTC") as TimeZone?
        
        return formatter.string(from: date)
    }
    
    
    static func transform(string:(String), toDateWithFormat format:(String)) -> Date{
        let formatter = DateFormatter.init()
        formatter.dateFormat = format
        formatter.timeZone   = NSTimeZone.init(name: "UTC") as TimeZone?
        
        return formatter.date(from: string)!
    }
    
    static func transform(date:(Date), toDateWithFormat format:(String)) -> Date{
        let dateST  = DManager.transform(date: date, toStringWithFormat: format)
        let resDate = DManager.transform(string: dateST, toDateWithFormat: format)
        
        return resDate
    }
    
    
    static func getMonthName(withNumber num: (Int)) -> String{
        switch (num) {
        case 1:
            return "january"
        case 2:
            return "february"
        case 3:
            return "march"
        case 4:
            return "april"
        case 5:
            return "may"
        case 6:
            return "june"
        case 7:
            return "july"
        case 8:
            return "august"
        case 9:
            return "september"
        case 10:
            return "october"
        case 11:
            return "november"
        case 12:
            return "december"
        default:
            return ".\(num)."
        }
    }
    
    static func getWeekName(withNumber num: (Int)) -> String{
        switch (num) {
        case 1:
            return "воскресенье"
        case 2:
            return "понедельник"
        case 3:
            return "вторник"
        case 4:
            return "среда"
        case 5:
            return "четверг"
        case 6:
            return "пятница"
        case 7:
            return "суббота"
        default:
            return ".\(num)."
        }
    }
    
    static func getShortWeekName(withNumber num: (Int)) -> String{
        switch (num) {
        case 1:
            return "ВС"
        case 2:
            return "ПН"
        case 3:
            return "ВТ"
        case 4:
            return "СР"
        case 5:
            return "ЧТ"
        case 6:
            return "ПТ"
        case 7:
            return "СБ"
        default:
            return ".\(num)."
        }
    }
    
    
    static func getWeekNumber(date: (Date)) -> Int {
        let gregorian = NSCalendar.init(calendarIdentifier: .gregorian)
        let components = gregorian?.components(.weekday, from: date)
        let num = components?.weekday
        
        return num!
    }
    
    
    static func getMonthNumber(date: (Date)) -> Int {
        let monthST : (NSString) =  NSString.init(string: DManager.transform(date: date, toStringWithFormat: "MM"))
        let monthI  = monthST.integerValue
        
        return monthI
    }
    
    
//    static func timeBetween(dateStart:(String), dateEnd:(String)) -> NSDateComponents{
//        let f = DateFormatter.init()
//        f.dateFormat = "yyyy-MM-dd HH-mm-ss"
//        f.locale = NSLocale.current
//
//        let startDate = f.date(from: dateStart)
//        let endDate   = f.date(from: dateEnd)
//
//        let calendar : (Calendar) = Calendar.init(identifier: .gregorian)
//        let components = calendar.dateComponents(<Calendar.Componen>, from: <#T##Date#>, to: <#T##Date#>)
//
//        return components
//    }
    
    
    static func timeIntervalBetween(time1: (Date), time2 : (Date)) -> Double{
        let startTimeST = DManager.transform(date: time1, toStringWithFormat: "HH:mm:ss")
        let endTimeST   = DManager.transform(date: time1, toStringWithFormat: "HH:mm:ss")
        
        let startDate   = DManager.transform(string: startTimeST, toDateWithFormat: "HH:mm:ss")
        let endDate     = DManager.transform(string: endTimeST, toDateWithFormat: "HH:mm:ss")
        
        return endDate.timeIntervalSince(startDate)
    }
    
    static func isDate(date: (Date), betweenDate1 date1: (Date), andDate2 date2: (Date)) -> Bool{
        if date.compare(date1) == .orderedAscending {
            return false
        }
        
        if date.compare(date2) == .orderedDescending {
            return false
        }
        
        return true
    }
    
    // MARK: - COPIE

    
    static func makeCopie(withLabel label:(UILabel)) -> UILabel{
        let newLabel = UILabel.init(frame: label.frame)
        
        newLabel.font               = label.font
        newLabel.textAlignment      = label.textAlignment
        newLabel.textColor          = label.textColor
        newLabel.autoresizingMask   = label.autoresizingMask
        
        return newLabel
    }
    
    
    static func makeCopie(withView view:(UIView)) -> UIView{
        let newView = UIView.init(frame: view.frame)
        
        newView.backgroundColor     = view.backgroundColor
        newView.autoresizingMask    = view.autoresizingMask
        newView.tag                 = view.tag
        
        return newView
    }
    
    
    static func makeCopie(withBtt btt:(UIButton)) -> UIButton{
        let newView = UIButton.init(frame: btt.frame)
        
        newView.backgroundColor     = btt.backgroundColor
        newView.autoresizingMask    = btt.autoresizingMask
        newView.tag                 = btt.tag
        newView.isHidden            = btt.isHidden
        
        newView.setTitle(btt.titleLabel?.text, for: .normal)
        newView.setBackgroundImage(btt.currentBackgroundImage, for: .normal)
        
        return newView
    }
    
    
    static func makeCopie(withTextFiled view:(UITextField)) -> UITextField{
        let newView = UITextField.init(frame: view.frame)
        
        newView.backgroundColor     = view.backgroundColor
        newView.autoresizingMask    = view.autoresizingMask
        newView.text                = view.text
        newView.placeholder         = view.placeholder
        newView.font                = view.font
        newView.textAlignment       = view.textAlignment
        newView.textColor           = view.textColor
        newView.tag                 = view.tag
        newView.isHidden            = view.isHidden
        newView.isUserInteractionEnabled = view.isUserInteractionEnabled
        
        return newView
    }
    
    
    static func makeCopie(withImageView view:(UIImageView)) -> UIImageView{
        let newView = UIImageView.init(frame: view.frame)
        
        newView.backgroundColor     = view.backgroundColor
        newView.autoresizingMask    = view.autoresizingMask
        newView.image               = view.image
        newView.tag                 = view.tag
        newView.contentMode         = view.contentMode
        
        return newView
    }
    
    
    
    // MARK: - ANIMATION
    
    
    static func show(view: (UIView), withFrame frame: (CGRect)){
        view.isHidden = false
        
        UIView.animate(withDuration: 0.3, animations: {
            view.frame = frame
            view.alpha = 1
        })
    }
    
    static func hide(view: (UIView), withFrame frame: (CGRect)){
        UIView.animate(withDuration: 0.3, animations: {
            view.frame = frame
            view.alpha = 0
        }) { (true) in
            view.isHidden = false
        }
    }
    
    
    static func isStringPropertly(string: (String?), orNotEmpty state:(Bool)) -> Bool{
        if let st = string{
            
            var newString = st.replacingOccurrences(of: " ", with: "")
            newString = newString.replacingOccurrences(of: "    ", with: "")
            newString = newString.replacingOccurrences(of: "\n", with: "")
            
            if (newString.isEmpty) {
                return false
            }
            
        } else {
            return false
        }
        
        return true
    }
    
    
    static func isExist(string: (String?), inString string2:(String?)) -> Bool{
        if let st1 = string, var st2 = string2{
            
            let lenght = st2.count
            
            st2 = st2.replacingOccurrences(of: st1, with: "")
            return st2.count != lenght
        }else{
            return false
        }
    }
    
    
    
    
    
    
}
