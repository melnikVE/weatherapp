//
//  UIManager.swift
//  SwiftProject
//
//  Created by Vlad Melnyk on 11.08.18.
//  Copyright © 2018 Vlad Melnyk. All rights reserved.
//

import UIKit

class UIManager: ViewController {


    // MARK: - FRAME
    
    static func statusBarFrame() -> CGRect {
        return UIApplication.shared.statusBarFrame;
    }
    
    
    // MARK: - UI
    
    static func setStatusBarStyle(style: (UIStatusBarStyle)) {
        UIApplication.shared.setStatusBarStyle(style, animated: false)
    }
    
    // MARK: - UI FRAME
    
    static func round(view: (UIView?), withRadius radius: (CGFloat) ) {
        if let currentView = view {
            currentView.layer.cornerRadius = radius
        }
    }
    
    static func round(view: (UIView?), withRadius radius: (CGFloat), maskToBounds: (Bool)) {
        if let currentView = view {
            currentView.layer.cornerRadius  = radius
            currentView.layer.masksToBounds = maskToBounds
        }
    }
    
    
    
    // MARK: - SHADOW
    
    static func makeShadow(forView view: (UIView?), withRadius radius:(CGFloat), opacity:(Float)) {
        if let currentView = view {
            currentView.layer.cornerRadius  = radius
            currentView.layer.opacity       = opacity
            currentView.layer.shadowColor   = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            currentView.layer.masksToBounds = false
        }
    }
    
    
    static func makeShadow(forView view: (UIView?), withRadius radius:(CGFloat), opacity:(Float), shadowRadius: (CGFloat), shadowOffset:(CGSize), color:(UIColor?)) {
        if let currentView = view {
            currentView.layer.cornerRadius  = radius
            currentView.layer.opacity       = opacity
            
            currentView.layer.masksToBounds = false
            currentView.layer.shadowRadius  = shadowRadius
            currentView.layer.shadowOffset  = shadowOffset
            
            if let currenctColor = color{
                currentView.layer.shadowColor = currenctColor.cgColor
            }else {
                currentView.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
        }
    }
    
    
    // MARK: - BORDER
    
    static func makeBorder(forView view:(UIView?), withColor: (UIColor?), width:(CGFloat)) {
        if let currentView = view {
            if let color = withColor {
                currentView.layer.borderColor  = color.cgColor
            }
            currentView.layer.borderWidth = width
        }
    }
    
    
    
    // MARK: - UDERLINE
    
//    static func makeUnderlineText(_ text: (String?)) -> NSAttributedString {
//        if let currentST = text {
//
//            let textRange = NSMakeRange(0, currentST.count)
//            let attributedText = NSMutableAttributedString(string: currentST)
//            attributedText.addAttribute(.underlineStyle , value: NSUnderlineStyle.styleSingle.rawValue, range: textRange)
//
//            return attributedText
//        }else{
//            return NSAttributedString(string: "")
//        }
//    }
    
    
//    static func makeUnderlineText(_ text: (String?), font:(UIFont)) -> NSAttributedString {
//        if let currentST = text {
//
//            let textRange = NSMakeRange(0, currentST.count)
//            let attributedText = NSMutableAttributedString(string: currentST)
//            attributedText.addAttribute(.underlineStyle , value: NSUnderlineStyle.styleSingle.rawValue, range: textRange)
//            attributedText.addAttribute(.font, value: font, range: textRange)
//
//            return attributedText
//        }else{
//            return NSAttributedString(string: "")
//        }
//    }
    
    
    
    // MARK: - COLOR
    
    static func randomColor() -> UIColor {
        return UIColor(red:   CGFloat(arc4random()) / CGFloat(UInt32.max),
                       green: CGFloat(arc4random()) / CGFloat(UInt32.max),
                       blue:  CGFloat(arc4random()) / CGFloat(UInt32.max),
                       alpha: 1.0)
    }
    
    
    
    
    
    
}
