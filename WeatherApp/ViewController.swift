//
//  ViewController.swift
//  WeatherApp
//
//  Created by Vlad Melnyk on 9/30/18.
//  Copyright © 2018 Vlad Melnyk. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var addNewCityBtt: UIButton!
    
    var weatherData : NSMutableArray?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        allocation()
        loadInfo()
        
    }
    
    
    func allocation(){
        
        tableView.dataSource = self
        tableView.delegate   = self
        
        weatherData = NSMutableArray.init()
        
        UIManager.round(view: addNewCityBtt, withRadius: 10)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if let data = weatherData{
            saveRecorder(data)
        }
    }
    
    
    func loadInfo() {
        
        var cityNames = ["Kyiv", "Odessa"]
        
        if let record : NSArray = self.getRecorder(){
            self.weatherData = NSMutableArray.init(array: record)
            self.tableView.reloadData()
            
            cityNames = [String]()
            
            for data in record {
                let cityData = (data as! NSDictionary).value(forKey: "city") as! NSDictionary
                let cityName = cityData.value(forKey: "name") as! String
                cityNames.append(cityName)
            }
            
        }
        
        if (!(cityNames.contains("Kyiv"))){
            cityNames.insert("Kyiv", at: 0)
        }
        
        if (!(cityNames.contains("Odessa"))){
            cityNames.insert("Odessa", at: 1)
        }
        
        
        for name : String in cityNames {
            API.init(baseURL: nil, sessionConfiguration: nil).getWeather(forCity: name, success: { (responce) in
                
                self.weatherData?.add(responce.data)
                
                if let record = self.weatherData{
                    
                    for data in record {
                        let cityData = (data as! NSDictionary).value(forKey: "city") as! NSDictionary
                        let cityName = cityData.value(forKey: "name") as! String
                    
                        if (self.isNameExistInRecord(name: cityName)){
                            self.weatherData?.replaceObject(at: record.index(of: data), with: data)
                        }else{
                            self.saveRecorder(record)
                        }
                        
                    }
                    
                    
                }
                
                self.tableView.reloadData()
                
            }) { (responce) in
                
                if let record = self.getRecorder(){
                    self.weatherData = NSMutableArray.init(array: record)
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func addNewCity(cityName city: (String)){
        API.init(baseURL: nil, sessionConfiguration: nil).getWeather(forCity: city, success: { (responce) in
            
            self.weatherData?.add(responce.data)
            
            if let record = self.weatherData{
                self.saveRecorder(record)
            }
            
            self.tableView.reloadData()
            
        }) { (responce) in
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailes" {
            let detailedController = segue.destination as! Detailed
            detailedController.weatherData = sender as? NSDictionary
        }
        
    }
    
    
    // MARK: - TABLE VIEW
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = weatherData?.count ?? 0
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! UITableViewCell
        
        let mainView            = cell.viewWithTag(1) as! UIView
        let cityLabel           = cell.viewWithTag(2) as! UILabel
        let temperatureLabel    = cell.viewWithTag(3) as! UILabel
        let descriptionLabel    = cell.viewWithTag(4) as! UILabel
        let descImageView       = cell.viewWithTag(5) as! UIImageView
        let detailedBtt         = cell.viewWithTag(6) as! UIButtonUp
        
        
        var data : NSDictionary = weatherData?[indexPath.row] as! NSDictionary
        let list : NSArray = data.value(forKey: "list") as! NSArray
        
        let cityData: NSDictionary = data.value(forKey: "city") as! NSDictionary
        let city : String = cityData.value(forKey: "name") as! String
        
        detailedBtt.data = data
        detailedBtt.addTarget(self, action: #selector(detailedBttDidClick(button:)), for: .touchUpInside)
        
        let calendar = NSCalendar.autoupdatingCurrent
        var currentDayWeather = [NSDictionary]()
    
        for weatherDict : Any in list {
            
            let weatherData : NSDictionary = weatherDict as! NSDictionary
            
            
            let dateST : String = weatherData.value(forKey: "dt_txt") as! String
            let date : Date  = DManager.transform(string: dateST, toDateWithFormat: "yyyy-MM-dd HH:mm:ss") as! Date
            
            if (calendar.isDateInToday(date)){
                currentDayWeather.append(weatherData)
                continue
            }
        }
        
        if let last = currentDayWeather.last {
            
            data = currentDayWeather.count > 2 ? currentDayWeather[2] : last
        
        }

        let mainData : NSDictionary = data.value(forKey: "main") as! NSDictionary
        let temperature = (mainData.value(forKey: "temp") as! NSNumber).floatValue

        let weatherDat  : NSDictionary   = (data.value(forKey: "weather")   as! NSArray).firstObject as! NSDictionary
        let descr       : String         = weatherDat.value(forKey: "main") as! String
        let descrIconST : String         = weatherDat.value(forKey: "icon") as! String
        var image       : UIImage?       = nil


        if let url = URL(string: "http://openweathermap.org/img/w/" + descrIconST + ".png") {
            URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                
                //print("RESPONSE FROM API: \(response)")
                if error != nil {
                    return
                }
                DispatchQueue.main.async {
                    if let data = data {
                        if let downloadedImage = UIImage(data: data) {
                            image = downloadedImage
                            descImageView.image   = image
                        }
                    }
                }
            }).resume()
        }

        // SET

        cityLabel.text = city
        temperatureLabel.text = "\(Int(temperature/32.0))" + "°"
        descriptionLabel.text = descr
        
        
        
        mainView.layer.cornerRadius     = 10
        mainView.layer.masksToBounds    = true
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 360
    }

    
    
    // MARK - ACTIONS
    
    @objc func detailedBttDidClick(button : UIButtonUp){
        let data : NSDictionary = (button.data)!
        
        self.performSegue(withIdentifier: "detailes", sender: data)
    }

    // MARK: - FILE OPERATIONS
    
    let recorderName = "WEATHER_RECORD"
    
    func getRecorder() -> NSArray?{
        let pathToFile = FManager.pathToFile(withName: recorderName)
        var file : NSArray?
        
        if let path = pathToFile {
            
            file = FManager.getInfoFromFile(withPath: path as URL, andFileType: .FileTypeMutableArray) as? NSArray
            
            
        }
        
        return file
    }
    
    func saveRecorder( _ record: (NSArray)){
        let pathToFile = FManager.pathToFile(withName: recorderName)
        
        if let path = pathToFile {
            
            record.write(to: path as URL, atomically: true)
            
        }
    }
    
    
    // MARK: - ACTIONS
    
    
    @IBAction func addNewCityBttDidClick(_ sender: UIButton) {
        
        let alert = UIAlertController.init(title: "Add new City", message: "Enter city name", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "City name"
            textField.textAlignment = NSTextAlignment.center
        }
        
        alert.addAction(UIAlertAction.init(title: "Save", style: .default, handler: { (action) in
            
            let textField = alert.textFields?.first
            
            if (!self.isNameExistInRecord(name: (textField?.text)!)) {
                self.addNewCity(cityName: (textField?.text)!)
            }
            
            
        }))
        
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .destructive, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func isNameExistInRecord(name: (String)) -> Bool{
        var cityNames = [String]()
        
        if let record : NSArray = self.getRecorder(){
            self.weatherData = NSMutableArray.init(array: record)
            self.tableView.reloadData()
            
            cityNames = [String]()
            
            for data in record {
                let cityData = (data as! NSDictionary).value(forKey: "city") as! NSDictionary
                let cityName = cityData.value(forKey: "name") as! String
                cityNames.append(cityName)
            }
            
        }
        
        if (cityNames.contains(name)) {
            return true
        }
        
        return false
    }
    
}

