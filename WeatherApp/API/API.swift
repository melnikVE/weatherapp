//
//  API.swift
//  WeatherApp
//
//  Created by Vlad Melnyk on 9/30/18.
//  Copyright © 2018 Vlad Melnyk. All rights reserved.
//

import UIKit
import AFNetworking

class API: AFHTTPSessionManager {

    let weatherToken = "d219ad29e64a259f9e8fe312a52c6da7"
    
    
    func getWeather(forCity city:(String), success:@escaping (_ responce : WebResponce) -> Void, failure:@escaping (_ responce: WebResponce) -> Void) {
        
//        let postST = "https://api.openweathermap.org/data/2.5/weather?q=" + city + "&APPID=" + weatherToken
        let postST = "https://api.openweathermap.org/data/2.5/forecast?q=" + city + "&APPID=" + weatherToken
        
        self.get(postST,
                 parameters: nil,
                 progress: { (progress) in
                    
        }, success: { (dataTask, responceObject) in
            let resp = WebResponce()
            
            if let responce = responceObject {
                
                let responceDictionary : NSDictionary = responce as! NSDictionary
                
                
                resp.data = responceDictionary
                
                
                success(resp)

            }
            
        }) { (dataTask, error) in
            
            let errorDescription = error.localizedDescription
            
            let resp = WebResponce()
            resp.errorMessage = errorDescription
            
            failure(resp)
            
            
        }
        
        
    }
    
    
}
