
//
//  Detailed.swift
//  WeatherApp
//
//  Created by Vlad Melnyk on 10/1/18.
//  Copyright © 2018 Vlad Melnyk. All rights reserved.
//

import UIKit

class Detailed: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temperaturelabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    public var weatherData : NSDictionary?
    public var weatherByDate : NSMutableArray?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        allocation()
        UISetUp()
    }
    
    
    // MARK: - ALLOCATION

    func allocation(){
        tableView.delegate   = self
        tableView.dataSource = self
        
        loadData()
        prepareWeatherByDate()
    }
    
    func UISetUp() {
        UIManager.round(view: mainView, withRadius: 15, maskToBounds: true)
    }
    
    func loadData(){
        
        var data : NSDictionary = weatherData!
        let list : NSArray = data.value(forKey: "list") as! NSArray
        
        let cityData: NSDictionary = data.value(forKey: "city") as! NSDictionary
        let city : String = cityData.value(forKey: "name") as! String
        
       
        
        let calendar = NSCalendar.autoupdatingCurrent
        var currentDayWeather = [NSDictionary]()
        
        for weatherDict : Any in list {
            
            let weatherData : NSDictionary = weatherDict as! NSDictionary
            
            
            let dateST : String = weatherData.value(forKey: "dt_txt") as! String
            let date : Date  = DManager.transform(string: dateST, toDateWithFormat: "yyyy-MM-dd HH:mm:ss") as! Date
            
            if (calendar.isDateInToday(date)){
                currentDayWeather.append(weatherData)
                continue
            }
        }
        
        if let last = currentDayWeather.last {
            data = currentDayWeather.count > 2 ? currentDayWeather[2] : last
        }
        
        
        
        let mainData : NSDictionary = data.value(forKey: "main") as! NSDictionary
        let temperature = (mainData.value(forKey: "temp") as! NSNumber).floatValue
        let humidity    = (mainData.value(forKey: "humidity") as! NSNumber).floatValue
        let pressure    = (mainData.value(forKey: "pressure") as! NSNumber).floatValue
        
        
        let weatherDat  : NSDictionary   = (data.value(forKey: "weather")   as! NSArray).firstObject as! NSDictionary
        let descr       : String         = weatherDat.value(forKey: "main") as! String
        let descrIconST : String         = weatherDat.value(forKey: "icon") as! String
        var image       : UIImage?       = nil
        
        
        if let url = URL(string: "http://openweathermap.org/img/w/" + descrIconST + ".png"){
            
            do {
                let data = try Data(contentsOf: url)
                image = UIImage.init(data: data)
            }catch {}
            
        }
        
        
        cityLabel.text = city
        temperaturelabel.text = "\(Int(temperature/32.0))" + "°"
        descriptionLabel.text = descr
//        descImageView.image   = image
        pressureLabel.text = "\(Int(pressure))" + " mm"
        humidityLabel.text = "\(Int(humidity))" + "%"
        
        
    }
    func prepareWeatherByDate(){
        
        weatherByDate = NSMutableArray.init()
        
        let data : NSDictionary = weatherData!
        let list : NSArray = data.value(forKey: "list") as! NSArray

        let twoDaysLater : Date = Date().addingTimeInterval(60*60*24*2)
        
        for weatherDict : Any in list {
            
            let weatherData : NSDictionary = weatherDict as! NSDictionary
            
            var calendar = NSCalendar.init(identifier: .gregorian)
            calendar?.timeZone = TimeZone.init(identifier: "UTC")!
            
            let dateST : String = weatherData.value(forKey: "dt_txt") as! String
            let date : Date  = DManager.transformToLocal(date: dateST, withFromat: "yyyy-MM-dd HH:mm:ss") as! Date
            
            
            if ((calendar?.isDateInToday(date))! && weatherByDate?.count == 0){
                weatherByDate?.add(weatherData)
                continue
            }
            
            if ((calendar?.isDateInTomorrow(date))! && weatherByDate?.count == 1){
                weatherByDate?.add(weatherData)
                continue
            }
            
            if ((calendar?.isDate(date, inSameDayAs: twoDaysLater))! && weatherByDate?.count == 2){
                weatherByDate?.add(weatherData)
                continue
            }
        }

        tableView.reloadData()
        
    }
    
    // MARK: - TABLE VIEW
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = weatherByDate?.count ?? 0
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! UITableViewCell
        
        let mainView             = cell.viewWithTag(1) as! UIView
        let dateLabel            = cell.viewWithTag(2) as! UILabel
        let temperatureLabel1    = cell.viewWithTag(3) as! UILabel
        let temperatureLabel2    = cell.viewWithTag(4) as! UILabel
    
        let data = weatherByDate?.object(at: indexPath.row) as! NSDictionary
        let dateST : String = data.value(forKey: "dt_txt") as! String
        let date : Date  = DManager.transform(string: dateST, toDateWithFormat: "yyyy-MM-dd HH:mm:ss") as! Date


        let mainData : NSDictionary = data.value(forKey: "main") as! NSDictionary
        let temperatureMax = (mainData.value(forKey: "temp_max") as! NSNumber).floatValue
        let temperatureMin = (mainData.value(forKey: "temp_min") as! NSNumber).floatValue


        let weatherDat  : NSDictionary   = (data.value(forKey: "weather")   as! NSArray).firstObject as! NSDictionary
        
        let dateMonth = DManager.getMonthName(withNumber: DManager.getMonthNumber(date: date))
        let dateDay = DManager.transform(date: date, toStringWithFormat: "d")
        let dateText = NSString.init(string: dateMonth).capitalized + "  " + dateDay 
        
        // SET

        dateLabel.text = dateText
        temperatureLabel1.text = "\(Int(temperatureMax/32.0))"
        temperatureLabel2.text = "\(Int(temperatureMin/32.0))"


        mainView.layer.cornerRadius     = 10
        mainView.layer.masksToBounds    = true
        
        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
}
